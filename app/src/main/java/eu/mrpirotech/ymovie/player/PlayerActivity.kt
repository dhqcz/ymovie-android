package eu.mrpirotech.ymovie.player

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.KeyEvent.KEYCODE_MEDIA_STOP
import android.view.SurfaceView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.media3.common.C
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.TrackSelectionOverride
import androidx.media3.common.Tracks
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import androidx.media3.ui.CaptionStyleCompat
import dagger.hilt.android.AndroidEntryPoint
import eu.mrpirotech.ymovie.databinding.ActivityPlayerBinding
import javax.inject.Inject


@UnstableApi
@AndroidEntryPoint
class PlayerActivity : AppCompatActivity() {

    val viewModel: PlayerViewModel by viewModels()
    @Inject lateinit var player: ExoPlayer

    companion object {
        fun createIntent(context: Context, url: Uri) =
            Intent(context, PlayerActivity::class.java).apply {
                data = url
            }

    }

    private val binding: ActivityPlayerBinding by lazy {
        ActivityPlayerBinding.inflate(layoutInflater)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        viewModel.langSettingsLiveData.observe(this) { langSettings ->
            player.trackSelectionParameters = player.trackSelectionParameters.buildUpon().apply {
                if (langSettings?.subtitles != null) {
                    player.currentTracks.groups.find {
                        it.type == C.TRACK_TYPE_TEXT && it.mediaTrackGroup.length > 0 && it.mediaTrackGroup.getFormat(
                            0
                        ).language == langSettings.subtitles
                    }?.also {
                        setOverrideForType(TrackSelectionOverride(it.mediaTrackGroup, 0))
                    }
                }
                if (langSettings?.audio != null) {
                    setPreferredAudioLanguage(langSettings.audio)
                }
            }.build()
        }
        setupPlayer()

    }

    private fun setupPlayer() {
        val mediaSource = ProgressiveMediaSource.Factory(DefaultHttpDataSource.Factory())
            .createMediaSource(MediaItem.fromUri(intent.data!!))
        //player.videoChangeFrameRateStrategy = VIDEO_CHANGE_FRAME_RATE_STRATEGY_OFF
        player.addListener(object : Player.Listener {
            override fun onTracksChanged(tracks: Tracks) {
                viewModel.onTracksChanged(tracks)
                super.onTracksChanged(tracks)
            }
        })
        binding.playerView.player = player
        player.setMediaSource(mediaSource)
        binding.playerView.setShowSubtitleButton(true)
        binding.playerView.keepScreenOn = true
        binding.playerView.subtitleView?.setStyle(
            CaptionStyleCompat(
                Color.WHITE,
                Color.TRANSPARENT,
                Color.TRANSPARENT,
                CaptionStyleCompat.EDGE_TYPE_OUTLINE,
                Color.BLACK,
                null
            )
        )
        binding.playerView.subtitleView?.setApplyEmbeddedStyles(false)
        (binding.playerView.videoSurfaceView as? SurfaceView)?.holder?.surface?.also {
            Log.d("XYZ", "surface available")
            // it.setFrameRate(23f, 1)
        }
        player.prepare()
        player.play()
    }

    override fun onPause() {
        player.pause()
        super.onPause()
    }

    override fun onDestroy() {
        player.release()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (binding.playerView.isControllerFullyVisible) {
            binding.playerView.hideController()
        } else {
            super.onBackPressed()
        }
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        Log.d("KEY", keyCode.toString())
        return when (keyCode) {
            KEYCODE_MEDIA_STOP -> {
                finish()
                true
            }

            else -> super.onKeyUp(keyCode, event)
        }

    }

}
