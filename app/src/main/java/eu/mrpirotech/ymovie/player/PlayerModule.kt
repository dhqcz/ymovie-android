package eu.mrpirotech.ymovie.player

import android.content.Context
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.DefaultRenderersFactory
import androidx.media3.exoplayer.ExoPlayer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext

@UnstableApi
@Module
@InstallIn(ActivityComponent::class)
class PlayerModule {
    @Provides
    fun player(@ActivityContext context: Context): ExoPlayer =
        ExoPlayer.Builder(context).setRenderersFactory(
            DefaultRenderersFactory(context).setExtensionRendererMode(
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON
            )
        ).build()
}
