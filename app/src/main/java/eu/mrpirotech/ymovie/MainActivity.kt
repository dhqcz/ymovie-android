package eu.mrpirotech.ymovie

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.KeyEvent.KEYCODE_ESCAPE
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import androidx.media3.common.util.UnstableApi
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.crashes.Crashes
import com.microsoft.appcenter.distribute.Distribute
import dagger.hilt.android.AndroidEntryPoint
import eu.mrpirotech.ymovie.databinding.ActivityMainBinding
import eu.mrpirotech.ymovie.player.PlayerActivity
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch


@UnstableApi @AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    private val URL = "https://ymovie.streamcinema.cz/tv.html"
    private val interceptedFlow = Channel<Uri>(Channel.BUFFERED)
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        if (BuildConfig.APPCENTER_SECRET.isNullOrBlank().not()) {
            AppCenter.start(
                application, BuildConfig.APPCENTER_SECRET,
                Crashes::class.java, Distribute::class.java
            )
        }
        lifecycleScope.launch {
            interceptedFlow.receiveAsFlow().distinctUntilChanged().collect {
                startActivity(PlayerActivity.createIntent(this@MainActivity, it))
                binding.webView.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KEYCODE_ESCAPE))
            }
        }
        setContentView(binding.root)
        binding.webView.apply {
            setBackgroundColor(Color.TRANSPARENT)
            visibility = View.VISIBLE
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            loadUrl(URL)
            webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
                ): Boolean {
                    if (request?.url?.toString()?.contains("wsfiles") == true) {
                        lifecycleScope.launch {
                            interceptedFlow.send(request.url)
                        }
                        return true
                    }
                    return super.shouldOverrideUrlLoading(view, request)
                }

                override fun shouldInterceptRequest(
                    view: WebView?,
                    request: WebResourceRequest?
                ): WebResourceResponse? {
                    Log.d("XYZ: request", "${request?.method} ${request?.url}")
                    if (request?.url?.toString()?.contains("wsfiles") == true) {
                        lifecycleScope.launch {
                            interceptedFlow.send(request.url)
                        }
                        return null
                    } else {
                        return super.shouldInterceptRequest(view, request)
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        Log.d("KEY", keyCode.toString())
        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        return super.onKeyUp(keyCode, event)
    }
}
