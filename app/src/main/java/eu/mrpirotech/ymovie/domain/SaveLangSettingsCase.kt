package eu.mrpirotech.ymovie.domain

import android.util.Log
import eu.mrpirotech.ymovie.data.store.LanguageStore
import javax.inject.Inject

class SaveLangSettingsCase @Inject constructor(
    private val languageStore: LanguageStore
) : BaseUseCase<SaveLangSettingsCase.Args, Unit>() {

    override suspend fun invoke(param: Args): Result<Unit> = kotlin.runCatching {
        languageStore.saveSubtitlesLanguage(param.subtitleLang)
        languageStore.saveAudioLanguage(param.audioLang)
        Log.d("XYZ:", "Langs saved $param")
    }

    data class Args(
        val subtitleLang: String?,
        val audioLang: String?
    )
}
