package eu.mrpirotech.ymovie.domain

import eu.mrpirotech.ymovie.data.model.LangSettings
import eu.mrpirotech.ymovie.data.store.LanguageStore
import javax.inject.Inject

class GetLangSettingsCase @Inject constructor(
    private val languageStore: LanguageStore
) : BaseUseCase<Unit, LangSettings>() {

    override suspend fun invoke(param: Unit): Result<LangSettings> = kotlin.runCatching {
        LangSettings(
            subtitles = languageStore.getSubtitlesLanguage(),
            audio = languageStore.getAudioLanguage()
        )
    }
}
