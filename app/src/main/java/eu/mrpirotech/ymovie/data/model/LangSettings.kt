package eu.mrpirotech.ymovie.data.model

data class LangSettings(
    val subtitles: String?,
    val audio: String?
)
